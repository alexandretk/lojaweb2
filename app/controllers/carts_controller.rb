class CartsController < ApplicationController

  before_filter :authenticate_normal_user!
  
  def add
    # slime the multiuser stuff
    @user = NormalUser.first
    if @user.cart == nil
      Rails.logger.info "CREATING NEW CART"
      @user.cart = Cart.new
    end
    cart_item = CartItem.new
    cart_item.product = Product.find params[:id]
    cart_item.cart = @user.cart
    @user.cart.add_to_cart cart_item

    flash[:notice] = "#{cart_item.product.name} added to cart."
    redirect_to carts_path
  end

  def clear
    @user = NormalUser.first
    @user.cart.destroy if @user.cart
    flash[:notice] = "Cleared cart."
    redirect_to carts_path
  end

  def index
    @cart = NormalUser.first.cart
  end

end