module ApplicationHelper

	  def cart_count
    if NormalUser.first.cart == nil
      return 0
    else
      return NormalUser.first.cart.items.count
    end
  end

end
