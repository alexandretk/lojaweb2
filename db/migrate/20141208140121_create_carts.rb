class CreateCarts < ActiveRecord::Migration
  def change
    create_table :carts do |t|
      t.integer :normal_user_id

      t.timestamps
    end
  end
end
