class CreateEncomendas < ActiveRecord::Migration
  def change
    create_table :encomendas do |t|
      t.string :endereco
      t.string :cartao_credito

      t.integer :cart_id

      t.timestamps
    end
  end
end
